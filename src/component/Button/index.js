/* eslint-disable */


import React from 'react';
import { View, Text,Image } from 'react-native';
import styles from './styles';
import Ripple from 'react-native-material-ripple';
import { useEffect } from 'react';
import { useState } from 'react';


function Button(props){

    const handleOnPress1 = ()=> {
        props.handleOnPress();
    }
    

    return(
            <Ripple style={[styles.btn,{backgroundColor: props.bgColor}]} onPress={handleOnPress1}>
                <Text style={styles.txtBtn}>{props.title}</Text>
            </Ripple>        
    );
}

export default Button;