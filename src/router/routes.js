/* eslint-disable */

const Routes={
    Login:'Login',
    SignUp:'SignUp',
    ResetPassword:'ResetPassword',
    Welcome:'Welcome',
    UnAuthenticated:'UnAuthenticated',
    Authenticated:'Authenticated',
    LoginWithFb:'LoginWithFb',
    ResetModal:'ResetModal',
    Home: 'Home',
}

export default Routes;