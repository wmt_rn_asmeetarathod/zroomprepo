/* eslint-disable */

import React from 'react';
import { StyleSheet } from 'react-native';

const styles=StyleSheet.create({
    mainContainer:{
        display:'flex',
        flex:1,
        justifyContent:'center',
        alignContent:'center',
        padding:20,
        // alignItems:'center',
    },
    vwModal:{
        backgroundColor:'white',
        padding: 35,
        margin:20,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 3
        },
        shadowOpacity: 1.0,
        shadowRadius: 4,
        elevation: 8,
    },
    txtModal:{
        color:'#898989',
        textAlign:'center',
        fontSize:18,
        
    },
    txtOkay:{
      
        color:'#5C7280',
        margin:10,
        fontSize:16,
        fontWeight:'bold',
    },
    btnOk:{
        display:'flex',
        justifyContent:'flex-end',
        alignContent:'flex-end',
        alignSelf:'flex-end',
        marginTop:20,
    }
});
export default styles;