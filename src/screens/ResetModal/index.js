/* eslint-disable */


import React from 'react';
import { View, Text, Modal, Alert } from 'react-native';
import styles from './styles';
import Ripple from 'react-native-material-ripple';


function ResetModal() {
    return (
        <View style={styles.mainContainer}>
            <Modal
                animationType='slide'
                transparent={false}
                visible={true}>
                <View style={styles.mainContainer}>
                    <View style={styles.vwModal}>
                        <Text style={styles.txtModal}>We've sent an email to reset your password so check your inbox and follow the link.</Text>
                        <Ripple style={styles.btnOk} onPress={() => Alert.alert('Okay!!')}>
                            <Text style={styles.txtOkay}>OKAY</Text>
                        </Ripple>
                    </View>
                </View>

            </Modal>
        </View>
    );
}

export default ResetModal;