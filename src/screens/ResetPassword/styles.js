/* eslint-disable */

import React from 'react';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    mainContainer: {
        display: 'flex',
        flexGrow:1,
        flexDirection: 'column',
        padding: 40,
        backgroundColor: 'white',
        justifyContent:'flex-start',
        alignContent:'center',
        
    },
    txtReset: {
        fontSize: 32,
        color: 'black',
        textAlign:'center',
        margin: 5,
    },
    txtDummy: {
        color: 'gray',
        fontSize: 16,
        margin: 2,
        textAlign:'center',
    },
    inpField: {
        fontSize: 18,
        borderBottomWidth: 2,
        borderBottomColor: "#E3E3E3",
        marginTop: 30,
        marginBottom:30,
    },
    btnOkay:{
       
        color:'#5C7280',
       
        fontSize:16,
        fontWeight:'bold',
    },

    modalMainContainer:{
        display:'flex',
        flex:1,
        justifyContent:'center',
        alignContent:'center',
        padding:20,
        // alignItems:'center',
    },
    vwModal:{
        backgroundColor:'white',
        padding: 35,
        margin:20,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 3
        },
        shadowOpacity: 1.0,
        shadowRadius: 4,
        elevation: 8,
    },
    txtModal:{
        color:'#898989',
        textAlign:'center',
        fontSize:18,
        
    },
    txtOkay:{
      
        color:'#5C7280',
        margin:10,
        fontSize:16,
        fontWeight:'bold',
    },
    btnOk:{
        display:'flex',
        justifyContent:'flex-end',
        alignContent:'flex-end',
        alignSelf:'flex-end',
        marginTop:20,
    }
});
export default styles;