/* eslint-disable */

import React,{useState} from 'react';
import { View, Text, TextInput, Alert, Modal, ImageBackground, Image } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import Button from '../../component/Button';
import styles from './styles';
import Ripple from 'react-native-material-ripple';
import TextFieldCompo from '../../component/TextFieldCompo';
import { validate } from '../../utils/validationUtils';

function ResetPassword(props) {
    const [rEmail,setREmail]=useState("");
    const [emailError,setEmailError] =useState("");

    const [modalvisible, setModalVisible] = useState(false);

    function validateReset(){
        const emailError = validate("email", rEmail);

        setEmailError(emailError);

        if(!emailError) {
            setModalVisible(true);
          }
    }

    return (
        
        <ScrollView 
            style={{flex:1,backgroundColor:'white'}} 
            contentContainerStyle={
                [
                    styles.mainContainer,
                    {backgroundColor: modalvisible ? 'rgba(0, 0, 0, 0.8)' : 'white'  }
                ]
            }
        >
            <Modal
                animationType="fade"
                transparent={true}
                visible={modalvisible}
                onRequestClose={()=>setModalVisible(!modalvisible)}>
                <View style={styles.modalMainContainer}>
                    <View style={styles.vwModal}>
                        <Text style={styles.txtModal}>We've sent an email to reset your password so check your inbox and follow the link.</Text>
                        <Ripple style={styles.btnOk} onPress={() => {
                            Alert.alert('Okay!!');
                            setModalVisible(false);
                            }}>
                            <Text style={styles.txtOkay}>OKAY</Text>
                        </Ripple>
                    </View>
                </View>
            </Modal>

            <View>
                <Text style={styles.txtReset}>Reset Password</Text>
                <Text style={styles.txtDummy}>Enter yout email and we'll send you a link to reset your password.</Text>
                
                 <TextFieldCompo
                       
                        placeholder="Email"
                        textContentType='emailAddress'
                        keyboardType='email-address'
                        onChangeText={(value)=>setREmail(value.trim())}
                        onBlur={()=>{
                        setEmailError(validate("email"),rEmail);
                    }}
                    error={emailError}
                    returnKeyType='next'
                    />
                <Button title="RESET PASSWORD" bgColor="#445D6D" handleOnPress={validateReset} />
            </View>
                           
        </ScrollView>
    );
}

export default ResetPassword;