/* eslint-disable */

import React from 'react';
import { StyleSheet } from 'react-native';
const styles=StyleSheet.create({
    mainContainer:{
        display:'flex',
        flexDirection:'column',
        flexGrow:1,   
        padding:20,
        justifyContent:'center',
        alignContent:'center',
        alignItems:'center',
        // backgroundColor:'purple',        

    },
    vwLogo:{
        display:'flex',
        flexGrow:3,
        margin:20,
        justifyContent:'center',
        alignContent:'center',
        alignItems:'center',
    },
    img:{
        width:300,
        height:100,
        resizeMode:'cover',
    },
    vwWelcome:{
        display:'flex',
        flexGrow:2,
        flexDirection:'column',
        justifyContent:'center',
        alignContent:'center',
        alignItems:'center',
    },
    txtWel:{
        fontSize:32,
        color:'black',
        margin:5,
    },
    txtDummy:{
        color:'gray',
        fontSize:16,
        margin:2,

    },
    vwButtons:{
        display:'flex',
        flexDirection:'column',
        // flex:1,
        flexGrow:5, 
        justifyContent:'flex-start',
        alignContent:'center',
        margin:10,
    },
});

export default styles;

