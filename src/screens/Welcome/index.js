/* eslint-disable */


import React from 'react';
import { View, Text, Image,ScrollView } from 'react-native';
import styles from './styles';
import Ripple from 'react-native-material-ripple';
import Button from '../../component/Button';



function Welcome(props) {

    const onLoginPress = ()=>{
        props.navigation.navigate("Login");
    }
    const onSignUpPress = ()=>{
        props.navigation.navigate("SignUp");
    }
    const onFacebookPress = ()=>{
        props.navigation.navigate("LoginWithFb");
    }

    return (
        
        <ScrollView style={{flex:1,backgroundColor:'white'}} contentContainerStyle={styles.mainContainer}>
            {/* <View style={styles.mainContainer}> */}
                <View style={styles.vwLogo}>
                    <Image style={styles.img} source={require('../../assets/zoom-logo1.jpg')} />
                </View>
                <View style={styles.vwWelcome}>
                    <Text style={styles.txtWel}>Welcome to Zroomp</Text>
                    <Text style={styles.txtDummy}>lorem ipsum dummytext here</Text>
                </View>
                <View style={styles.vwButtons}>
                    <Button title="LOGIN" bgColor="#708C9F" handleOnPress={onLoginPress} />
                    <Button title="SIGNUP" bgColor="#708C9F" handleOnPress={onSignUpPress}/>
                    <Button title="LOGIN WITH FACEBOOK" bgColor="#3B5998" handleOnPress={onFacebookPress}/>
                </View>
            {/* </View> */}
        </ScrollView>
        
    );
}

export default Welcome;