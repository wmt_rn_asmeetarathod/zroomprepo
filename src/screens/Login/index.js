/* eslint-disable */

import React, { useRef } from 'react';
import { useState, createRef, forwardRef } from 'react';
import { View, Text, Image, Alert, Keyboard, TextInput,ScrollView } from 'react-native';

import Button from '../../component/Button';
import styles from '../Login/styles';
import TextFieldCompo from '../../component/TextFieldCompo';
import { validate } from '../../utils/validationUtils';

const Login=(props)=> {
    
    const [fEmail, setFEmail] = useState("");
    const [emailError, setEmailError] = useState("");
    const [fPassword, setFPassword] = useState("");
    const [passwordError, setPasswordError] = useState("");


    const refEmail=useRef();
    console.log("refEmail ==>> ",refEmail);

    const refPswd=useRef();
    console.log("refPswd ==>> ",refPswd);
    // refArr["refEmail"] = createRef();
    // refArr["refPswd"] = createRef();
    // console.log("refs before ==>> ", refArr);

    function validateLogin() {
        const emailError = validate("email", fEmail)
        const passwordError = validate("password", fPassword)

        setEmailError(emailError);
        setPasswordError(passwordError);

        if (!emailError && !passwordError) {
            Alert.alert("Details are valid!");
        }
    }

    console.log("refPswd.current ==>> ",refPswd.current);
    return (
        <ScrollView style={{ flex: 1, backgroundColor: 'white' }} contentContainerStyle={styles.mainContainer}>
            {/* <View style={styles.mainContainer}>  */}
            <View style={styles.vwLogo}>
                <Image style={styles.loginLogo} source={require('../../assets/ic_woken_token_round_transparent.png')} />
                <Text style={styles.txtLoginTo}>Login to Zroomp</Text>
                <Text style={styles.txtDummy}>lorem ipsum dummytext here</Text>
            </View>

            <View style={styles.vwFields}>

                <TextFieldCompo
                    ref={refEmail}
                    placeholder="Enter Email ID"
                    textContentType='emailAddress'
                    keyboardType='email-address'
                    autoFocus={true}
                    onChangeText={(value) => setFEmail(value.trim())}
                    onBlur={() => {
                        setEmailError(validate("email"), fEmail);
                    }}
                    error={emailError}
                    returnKeyType='next'
                    nextFocus="refPswd"
                    onSubmitEditing={() => refPswd.current.focus()}
                    // handleFocus={() => focusNextInput("refPswd")}
                />

                <TextFieldCompo
                    // () => refs["refPswd"] = createRef()
                    // inputRef={refArr["refPswd"]}

                    // fieldRef={refs["refPswd"]}
                    // inputRef={el => refArr["refPswd"]= el}
                    ref={refPswd}
                    placeholder="Enter Password"
                    textContentType='password'
                    secureTextEntry={true}

                    onChangeText={(value) => setFPassword(value.trim())}
                    onBlur={() => {
                        setPasswordError(validate("password"), fPassword);
                    }}
                    error={passwordError}
                    returnKeyType='done'
                    onSubmitEditing={() => Keyboard.dismiss()}
                    // handleFocus={() => Keyboard.dismiss()}
                />

            </View>
            <View style={styles.vwButton}>
                <Button title="LOGIN" bgColor="#708C9F" handleOnPress={validateLogin} />
                <Text style={styles.txtForgot} onPress={() => {
                    props.navigation.navigate("ResetPassword");
                }}>Forgot your password?</Text>
            </View>

            {/* </View> */}
        </ScrollView>
    );
}

export default Login;