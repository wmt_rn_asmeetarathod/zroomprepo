/* eslint-disable */

import { StyleSheet } from 'react-native';
const styles=StyleSheet.create({
    mainContainer:{
        display:'flex',
        flexDirection:'column',
        flexGrow:1,
        justifyContent:'center',
        alignContent:'center',
        backgroundColor:'white',
        padding:30,
    },
    loginLogo:{
        height:150,
        width:150,
        resizeMode:'cover',
        margin:20,
    },
    txtLoginTo:{
        fontSize:32,
        color:'black',
        margin:5,
    },
    txtDummy:{
        color:'gray',
        fontSize:16,
        margin:2,

    },
    inpEmail:{
        fontSize:20,
        borderBottomWidth: 2, 
        borderBottomColor: "#E5E9EB"
    },
    inpPswd:{
        fontSize:20,
        borderBottomWidth: 2, 
        borderBottomColor: "#E5E9EB"
    },
    vwLogo:{
        display:'flex',
        flexGrow:1,
        flexDirection:'column',
        justifyContent:'center',
        alignContent:'center',
        alignItems:'center',
    },
    vwFields:{
        display:'flex',
        flexGrow:2,
        flexDirection:'column',
        justifyContent:'center',
        alignContent:'center',
    },
    vwButton:{
        display:'flex',
        flexGrow:1,
        flexDirection:'column',
        justifyContent:'center',
        alignContent:'center',
    },
    txtForgot:{
        color:'grey',
        textAlign:'right',
        margin:5,
        fontSize:15,
        margin:10,
    },
})

export default styles;