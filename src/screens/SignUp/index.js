/* eslint-disable */

import React,{createRef,useState,useRef} from 'react';
import { View, Text,Alert,ScrollView ,Keyboard} from 'react-native';
import Button from '../../component/Button';
import styles from './styles';
import TextFieldCompo from '../../component/TextFieldCompo';
import { equalityCheck, validate } from '../../utils/validationUtils';
import { useTabIndex } from 'react-tabindex';

function SignUp() {



    const tabPhone=useTabIndex();
    const tabEmail=useTabIndex();
    const tabPswd=useTabIndex();

    const refPhone=useRef();
    const refEmail=useRef();
    const refPswd=useRef();
    const refConfirmPswd=useRef();

    const [phoneNo,setPhoneNo] =useState("");
    const [phoneError,setPhoneError]=useState("");

    const [sEmail,setSEmail]=useState("");
    const [emailError,setEmailError] =useState("");

    const [sPassword,setSPassword]=useState("");
    const [passwordError,setPasswordError] =useState("");

    const [sConfirmPassword,setSConfirmPassword]=useState("");
    const [confirmPasswordError,setConfirmPasswordError] =useState("");

    const [equalError,setEqualError]=useState("");

    function validateSignUp(){
        


        const phoneError = validate("phoneNo", phoneNo);
        const emailError = validate("email", sEmail);
        const passwordError = validate("password",sPassword);
        const confirmPasswordError=validate("confirmPassword",sConfirmPassword);
        const equalError=equalityCheck(sPassword,sConfirmPassword);

        setPhoneError(phoneError);
        setEmailError(emailError);
        setPasswordError(passwordError);
        setConfirmPasswordError(confirmPasswordError);

        if(equalError){
            if(!phoneError && !emailError && !passwordError && !confirmPasswordError) {
                Alert.alert("SignUp Successfully");
              }
        }
        
    }

    return (
        <ScrollView style={{flex:1,backgroundColor:'white'}} contentContainerStyle={styles.mainContainer}>
            {/* <View style={styles.mainContainer}> */}
                <View style={styles.vwFirst}>

                    <Text style={styles.txtSignUp}>Signup to Zroomp</Text>
                    <Text style={styles.txtDummy}>lorem ipsum dummytext here</Text>

                </View>
                <View style={styles.vwFields}>
                    <TextFieldCompo
                        ref={refPhone}
                        placeholder="Phone Number"
                        textContentType='telephoneNumber'
                        keyboardType='number-pad'
                        tabIndex={tabPhone}
                        onChangeText={(value)=>setPhoneNo(value.trim())}
                        onBlur={()=>{
                        setPhoneError(validate("phoneNo"),phoneNo);
                    }}
                    error={phoneError}
                    returnKeyType='next'
                    onSubmitEditing={() => refEmail.current.focus()}
                    />

                    <TextFieldCompo
                        ref={refEmail}
                        placeholder="Email"
                        textContentType='emailAddress'
                        keyboardType='email-address'
                        tabIndex={tabEmail}
                        onChangeText={(value)=>setSEmail(value.trim())}
                        onBlur={()=>{
                        setEmailError(validate("email"),sEmail);
                        
                    }}

                    error={emailError}
                    returnKeyType='next'
                    onSubmitEditing={() => refPswd.current.focus()}
                    />
                    <TextFieldCompo
                        ref={refPswd}
                        placeholder="Password"
                        textContentType='password'
                        secureTextEntry={true}
                        onChangeText={(value)=>setSPassword(value.trim())}
                        onBlur={()=>{
                        setPasswordError(validate("password"),sPassword);
                    }}
                    error={passwordError}
                    returnKeyType='next'
                    onSubmitEditing={() => refConfirmPswd.current.focus()}
                    />
                    <TextFieldCompo
                        ref={refConfirmPswd}
                        placeholder="Confirm Password"
                        textContentType='password'
                        secureTextEntry={true}
                        onChangeText={(value)=>setSConfirmPassword(value.trim())}
                        onBlur={()=>{
                        setConfirmPasswordError(validate("confirmPassword"),sConfirmPassword);
                    }}
                    error={confirmPasswordError}
                    returnKeyType='done'
                    onSubmitEditing={() => Keyboard.dismiss()}
                    />


                </View>
                <View style={styles.vwButton}>
                    
                    <Button title="SIGNUP" bgColor="#708C9F" handleOnPress={validateSignUp}/>
                </View>
            {/* </View> */}
        </ScrollView>
    );
}


export default SignUp;