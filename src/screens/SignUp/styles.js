/* eslint-disable */

import React from 'react';
import { StyleSheet } from 'react-native';

const styles=StyleSheet.create({
    mainContainer:{
        display:'flex',
        flexGrow:1,
        flexDirection:'column',
        padding:40,
        backgroundColor:'white',
    },
    txtSignUp:{
        fontSize:32,
        color:'black',
        
        margin:5,
    },
    txtDummy:{
        color:'gray',
        fontSize:16,
        margin:2,

    },
    inpField:{
        fontSize:18,
        borderBottomWidth:2,
        borderBottomColor:"#E3E3E3",
        margin:5,
    },
    vwFirst:{
        display:'flex',
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center',
        alignContent:'center',
        
        flexGrow:1,
    },
    vwFields:{
        display:'flex',
        flexDirection:'column',
        justifyContent:'center',
        // alignItems:'center',
        alignContent:'center',
        flexGrow:4,
        
    },
    vwButton:{
        display:'flex',
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center',
        alignContent:'center',
        
    }
})
export default styles;