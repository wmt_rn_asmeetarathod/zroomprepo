/* eslint-disable */

import validatejs from 'validate.js';

export function validate(fieldName, value) {

  const formValues = {}
  formValues[fieldName] = value;

  const formFields = {}
  formFields[fieldName] = validation[fieldName];

  const result = validatejs(formValues, formFields);

  if (result) {
    return result[fieldName][0];
  }
  return null;
}

export const validation = {
  phoneNo: {
    presence: {
      message: "^Please enter an Phone Number"
    },
    length: {
      minimum: 10,
      message: "^Your Phone Number must be 10 Digits"
    }
  },
  email: {
    presence:
      true
    ,
    email: {
      message: "^Please enter a valid email address"
    }
  },
  password: {
    presence: true
    ,
    length: {
      minimum: 5,
      message: "^Your password must be at least 5 characters"
    }
  },
  confirmPassword: {

    presence: {
      message: "^Please enter a password"
    },
    length: {
      minimum: 5,
      message: "^Your password must be at least 5 characters"
    }
  }
}


export function equalityCheck(pswd, confirmPswd) {
  console.log("pswd === confirmPswd ==>>" ,pswd === confirmPswd);
  if (pswd === confirmPswd) {
    return true;
    
  }
  else {
    return false;
  }
}