/* eslint-disable */
import 'react-native-gesture-handler';
import React from 'react';
import type { Node } from 'react';
import {
  SafeAreaView, StatusBar, View
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Routes from './src/router/routes';
import Welcome from './src/screens/Welcome';
import Login from './src/screens/Login';
import SignUp from './src/screens/SignUp';
import ResetPassword from './src/screens/ResetPassword';
import LoginWithFb from './src/screens/LoginWithFb';
import IonIcon from 'react-native-vector-icons';
import ResetModal from './src/screens/ResetModal';
import Home from './src/screens/Home';


const unAuthenticatedStack = createStackNavigator();
function UnAuthenticatedStack() {
  return (
    <>
      <unAuthenticatedStack.Navigator initialRouteName={Routes.Welcome} >

        <unAuthenticatedStack.Screen name={Routes.Welcome} component={Welcome} options={{
          headerShown: false,
        }} />
        <unAuthenticatedStack.Screen name={Routes.Login} component={Login} options={{
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "#708C9F",
          },
          // headerLeft:()=> <IonIcon name="chevron-back-outline" color="white" size={25} />
        }} />
        <unAuthenticatedStack.Screen name={Routes.SignUp} component={SignUp} options={{
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "#708C9F",
          },
        }} />
        <unAuthenticatedStack.Screen name={Routes.ResetPassword} component={ResetPassword} options={{
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "#708C9F",
          },
        }} />
        <unAuthenticatedStack.Screen name={Routes.LoginWithFb} component={LoginWithFb} options={{
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "#708C9F",
          },
        }} />

      </unAuthenticatedStack.Navigator>
    </>
  );
}

const authenticatedStack = createStackNavigator();
function AuthenticatedStack(){
  return(
    <authenticatedStack.Navigator>
      <authenticatedStack.Screen name={Routes.Home} component={Home} />
    </authenticatedStack.Navigator>
  )
}

const Stack = createStackNavigator();

const App: () => Node = () => {
  return (
    <>
      <NavigationContainer>
        <StatusBar barStyle="light-content" backgroundColor='#445D6D' />
        <Stack.Navigator initialRouteName={Routes.Authenticated}>

          <Stack.Screen name={Routes.UnAuthenticated} component={UnAuthenticatedStack} options={{ headerShown: false }} />
          <Stack.Screen name={Routes.Authenticated} component={AuthenticatedStack}  options={{ headerShown: false }}/>
        </Stack.Navigator>

      </NavigationContainer>
    </>
  );
}


export default App;
